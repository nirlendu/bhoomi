/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const campaignJson = {
     "campaignId": "TEST_CAMPAIGN_ID",
     "campaignDetails": "TEST CAMPAIGN!",
     "validFrom": 1555522613904,
     "validTill": 1556532613904
}

const campaignModifiedJson = {
     "campaignId": "TEST_CAMPAIGN_ID",
     "campaignDetails": "TEST CAMPAIGN MODIFIED!",
     "validFrom": 1555522613904,
     "validTill": 1556532613904
}

describe('Business', function() {
	it('for successful operation', function(done) {
		// FIRST - INSERT
		request
		.post('/campaign/insert')
		.send(campaignJson)
		.end(function(err, result) {
			assert.equal(result.body.id, campaignJson.campaignId);
			// SECOND - GET
			request
			.get('/campaign/get/' + campaignJson.campaignId)
			.end(function(err, result) {
				assert.equal(result.body.campaignId, campaignJson.campaignId);
				assert.equal(result.body.campaignDetails, campaignJson.campaignDetails);
				assert.equal(result.body.validFrom, campaignJson.validFrom);
				assert.equal(result.body.validTill, campaignJson.validTill);
				// THIRD - UPDATE
				request
				.post('/campaign/update')
				.send(campaignModifiedJson)
				.end(function(err, result) {
					assert.equal(result.body.id, campaignModifiedJson.campaignId);
					// FOURTH - GET
					request
					.get('/campaign/get/' + campaignModifiedJson.campaignId)
					.end(function(err, result) {
						assert.equal(result.body.campaignId, campaignModifiedJson.campaignId);
						assert.equal(result.body.campaignDetails, campaignModifiedJson.campaignDetails);
						assert.equal(result.body.validFrom, campaignModifiedJson.validFrom);
						assert.equal(result.body.validTill, campaignModifiedJson.validTill);
						// FIFTH - DELETE
						request
						.delete('/campaign/delete/' +  campaignModifiedJson.campaignId)
						.end(function(err, result) {
							assert.equal(result.body.id, campaignModifiedJson.campaignId);
							// SIXTH - GET
							request
							.get('/campaign/get/' + campaignModifiedJson.campaignId)
							.end(function(err, result) {
								assert.equal(result.body.campaignId, null);
								assert.equal(result.body.campaignDetails, null);
								assert.equal(result.body.validFrom, null);
								assert.equal(result.body.validTill, null);
								done();
							})
						})
					})
				})
			})
		})
	});
});