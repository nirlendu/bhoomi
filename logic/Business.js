/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const businessJson = {
     "businessId": "TEST_B",
     "businessName": "TEST BUSINESS!",
     "messageId": "TEST_B"
}

const businessModifiedJson = {
     "businessId": "TEST_B",
     "businessName": "TEST MODIFIED BUSINESS!",
     "messageId": "TEST_B"
}

describe('Business', function() {
	it('for successful operation', function(done) {
		// FIRST - INSERT
		request
		.post('/business/insert')
		.send(businessJson)
		.end(function(err, result) {
			assert.equal(result.body.id, businessJson.businessId);
			// SECOND - GET
			request
			.get('/business/get/' + businessJson.businessId)
			.end(function(err, result) {
				assert.equal(result.body.businessId, businessJson.businessId);
				assert.equal(result.body.businessName, businessJson.businessName);
				assert.equal(result.body.messageId, businessJson.messageId);
				// THIRD - UPDATE
				request
				.post('/business/update')
				.send(businessModifiedJson)
				.end(function(err, result) {
					assert.equal(result.body.id, businessModifiedJson.businessId);
					// FOURTH - GET
					request
					.get('/business/get/' + businessModifiedJson.businessId)
					.end(function(err, result) {
						assert.equal(result.body.businessId, businessModifiedJson.businessId);
						assert.equal(result.body.businessName, businessModifiedJson.businessName);
						assert.equal(result.body.messageId, businessModifiedJson.messageId);
						// FIFTH - DELETE
						request
						.delete('/business/delete/' + businessModifiedJson.businessId)
						.end(function(err, result) {
							assert.equal(result.body.id, businessModifiedJson.businessId);
							// SIXTH - GET
							request
							.get('/business/get/' + businessModifiedJson.businessId)
							.end(function(err, result) {
								assert.equal(result.body.businessId, null);
								assert.equal(result.body.businessName, null);
								assert.equal(result.body.messageId, null);
								done();
							})
						})
					})
				})
			})
		})
	});
});