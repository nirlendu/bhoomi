/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const authJson = {
	"businessId" : "TEST_B",
	"username": "TEST_BUSINESS",
	"name": "TEST BUSINESS",
	"password": "test"
}

const authModifiedJson = {
	"businessId" : "TEST_B",
	"username": "TEST_BUSINESS",
	"name": "TEST BUSINESS UPDATE!"
}

const authPasswordJson = {
	"businessId" : "TEST_B",
	"password": "new test!"
}

describe('Auth', function() {
	it('for successful operation', function(done) {
		// FIRST - INSERT
		request
		.post('/auth/insert')
		.send(authJson)
		.end(function(err, result) {
			assert.equal(result.body.id, authJson.businessId);
			// SECOND - GET
			request
			.get('/auth/get/' + authJson.username + '/' + authJson.password)
			.end(function(err, result) {
				assert.equal(result.body.name, authJson.name);
				assert.equal(result.body.businessId, authJson.businessId);
				// THIRD - UPDATE
				request
				.post('/auth/update')
				.send(authModifiedJson)
				.end(function(err, result) {
					assert.equal(result.body.id, authModifiedJson.businessId);
					// FOURTH - GET
					request
					.get('/auth/get/' + authModifiedJson.username + '/' + authJson.password)
					.end(function(err, result) {
						assert.equal(result.body.name, authModifiedJson.name);
						assert.equal(result.body.businessId, authModifiedJson.businessId);
						// FIFTH - UPDATE PASSWORD
						request
						.post('/auth/update-password')
						.send(authPasswordJson)
						.end(function(err, result) {
							assert.equal(result.body.id, authPasswordJson.businessId);
							// SIXTH - GET
							request
							.get('/auth/get/' + authModifiedJson.username + '/' + authPasswordJson.password)
							.end(function(err, result) {
								assert.equal(result.body.name, authModifiedJson.name);
								assert.equal(result.body.businessId, authModifiedJson.businessId);
								// SEVENTH - DELETE
								request
								.delete('/auth/delete/' + authPasswordJson.businessId)
								.end(function(err, result) {
									assert.equal(result.body.id, authPasswordJson.businessId);
									// SIXTH - GET
									request
									.get('/auth/get/' + authModifiedJson.username + '/' + authPasswordJson.password)
									.end(function(err, result) {
										assert.equal(result.body.businessId, null);
										done();
									})
								})
							})
						})
					})
				})
			})
		})
	});
});