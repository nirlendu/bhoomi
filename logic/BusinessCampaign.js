/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const businessId = "1";
const campaignId = "TEST_CAMPAIGN_ID";
const campaignModifedId = "TEST_CAMPAIGN_ID_2"

const campaignList = [];
campaignList.push(campaignId);

const campaignModifiedList = [];
campaignModifiedList.push(campaignId);


describe('Business Campaign', function() {
	it('for successful operation', function(done) {
		// FIRST - INSERT
		request
		.get('/business-campaign/insert/' + businessId + '/' + campaignId)
		.end(function(err, result) {
			assert.equal(result.body.id, businessId);
			// SECOND - GET
			request
			.get('/business-campaign/get-campaigns/' + businessId)
			.end(function(err, result) {
				assert.equal(result.body.length, 1);
				request
				.get('/business-campaign/get-businesses/' + campaignId)
				.end(function(err, result) {
					assert.equal(result.body.length, 1);
					// THIRD - UPDATE
					request
					.get('/business-campaign/update/' + businessId + '/' + campaignModifedId)
					.end(function(err, result) {
						assert.equal(result.body.id, businessId);
						// FOURTH - GET
						request
						.get('/business-campaign/get-campaigns/' + businessId)
						.end(function(err, result) {
							assert.equal(result.body.length, 1);
							// FIFTH - DELETE
							request
							.delete('/business-campaign/delete/' +  businessId + '/' + campaignModifedId)
							.end(function(err, result) {
								assert.equal(result.body.id, businessId);
								// SIXTH - GET
								request
								.get('/business-campaign/get-campaigns/' + businessId)
								.end(function(err, result) {
									assert.equal(result.body.length, 0);
									done();
								})
							})
						})
					})
				})
			})
		})
	});
});