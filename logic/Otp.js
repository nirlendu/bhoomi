/**
 * Copyright - Panally Internet
 */
 
const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const phoneNumber = '9990426229';

describe('Otp', function() {
	it('for successful operation', function(done) {
		request
		.get('/otp/get/' + phoneNumber)
		.end(function(err, result) {
			request
			.get('/otp/verify/' + phoneNumber + '/' + result.body.password)
			.end(function(err, result) {
				assert.equal(result.body.check, true);
				done();
			})
		})
	});
	it('for un-successful operation', function(done) {
		request
		.get('/otp/get/' + phoneNumber)
		.end(function(err, result) {
			request
			.get('/otp/verify/' + phoneNumber + '/' + 'pwd')
			.end(function(err, result) {
				assert.equal(result.body.check, false);
				done();
			})
		})
	});
});