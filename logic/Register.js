/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const registerJson = {
    "businessId": "1",
    "phoneNumber": "9990426229",
}

describe('Register', function() {
	it('for successful operation', function(done) {
		// FIRST INSERT
		request
		.post('/register/insert')
		.send(registerJson)
		.end(function(err, result) {
			assert.equal(result.body.id, registerJson.phoneNumber);
			// SECOND GET
			request
			.get('/register/get-recent/' + registerJson.businessId + '/1')
			.end(function(err, result) {
				assert.equal(result.body.length, 1);
				// THIRD DELETE
				request
				.delete('/register/delete/' +  registerJson.businessId + '/' + registerJson.phoneNumber)
				.end(function(err, result) {
					assert.equal(result.body.id, registerJson.phoneNumber);
					done();
				})
			})
		})
	});
});
