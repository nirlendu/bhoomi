/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const referralJson = {
	"businessId": "1",
	"phoneNumber": "9988998899",
	"refPhoneNumber": "9988998899"
}

describe('Referral', function() {
	it('for successful operation', function(done) {
		// FIRST - INSERT
		request
		.post('/referral/insert')
		.send(referralJson)
		.end(function(err, result) {
			assert.equal(result.body.id, referralJson.phoneNumber);
			// SECOND - DELETE
			request
			.delete('/referral/delete/' +  referralJson.businessId + '/' + referralJson.phoneNumber + '/' + referralJson.refPhoneNumber)
			.end(function(err, result) {
				assert.equal(result.body.id, referralJson.phoneNumber);
				done();
			})
		})
	});
});