/**
 * Copyright - Panally Internet
 */
 
const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const validRegisterJson = {
	"businessId": "1",
	"phoneNumber": "9988998896"
}

const validReferralJson = {
	"businessId": "1",
	"phoneNumber": "9988998895",
	"refPhoneNumber": "9988998896"
}

describe('Register', function() {
	it('for successful operation', function(done) {
		// FIRST INSERT REGISTER
		request
		.post('/register/insert')
		.send(validRegisterJson)
		.end(function(err, result) {
			assert.equal(result.body.id, validRegisterJson.phoneNumber);
			// SECOND VALIDATE CORE REFERRAL
			request
			.post('/core/validate/referral')
			.send(validReferralJson)
			.end(function(err, result) {
				assert.equal(result.body.phoneNumber, validReferralJson.phoneNumber);
				// THIRD VALIDATE OTP
				let validReferralJsonTmp = validReferralJson;
				validReferralJsonTmp.password = result.body.password;
				// FOURTH INSERT REFERRAL
				request
				.post('/core/insert/referral')
				.send(validReferralJsonTmp)
				.end(function(err, result) {
					assert.equal(result.body.businessId, validReferralJson.businessId);
					assert.equal(result.body.phoneNumber, validReferralJson.phoneNumber);
					// FIFTH DELETE REGISTER FOR 1
					request
					.get('/core/validate/redeem/' + validRegisterJson.businessId + '/' + validRegisterJson.phoneNumber)
					.end(function(err, result) {
					assert.equal(result.body.phoneNumber, validRegisterJson.phoneNumber);
						// FIFTH DELETE REGISTER FOR 1
						let jsonTmp = validRegisterJson;
 						jsonTmp.password = result.body.password;
 						request
						.post('/core/insert/redeem')
						.send(jsonTmp)
						.end(function(err, result) {
							assert.equal(result.body.id, validRegisterJson.phoneNumber);
							// FIFTH DELETE REGISTER FOR 1
							request
							.delete('/register/delete/' +  validRegisterJson.businessId + '/' + validRegisterJson.phoneNumber)
							.end(function(err, result) {
								assert.equal(result.body.id, validRegisterJson.phoneNumber);
								// SIXTH DELETE REGISTER FOR 2
								request
								.delete('/register/delete/' +  validReferralJson.businessId + '/' + validReferralJson.phoneNumber)
								.end(function(err, result) {
									assert.equal(result.body.id, validReferralJson.phoneNumber);
									// SEVENTH DELETE REFERRAL
									request
									.delete('/referral/delete/' +  validReferralJson.businessId + '/' + validReferralJson.phoneNumber + '/' + validReferralJson.refPhoneNumber)
									.end(function(err, result) {
										assert.equal(result.body.id, validReferralJson.phoneNumber);
										done();
									})
								})
							})
						})
					})
				})
			})
		})
	});
});

