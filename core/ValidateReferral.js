/**
 * Copyright - Panally Internet
 */

const request = require('supertest')('http://localhost:9000/v1');
const assert = require('assert');

const validRegisterJson = {
	"businessId": "1",
	"phoneNumber": "9988998899"
}

const validReferralJson = {
	"businessId": "1",
	"phoneNumber": "9988998898",
	"refPhoneNumber": "9988998899"
}

describe('Otp', function() {
	it('for successful operation', function(done) {
		// FIRST INSERT REGISTER
		request
		.post('/register/insert')
		.send(validRegisterJson)
		.end(function(err, result) {
			assert.equal(result.body.id, validRegisterJson.phoneNumber);
			// SECOND VALIDATE CORE REFERRAL
			request
			.post('/core/validate/referral')
			.send(validReferralJson)
			.end(function(err, result) {
				assert.equal(result.body.phoneNumber, validReferralJson.phoneNumber);
				// THIRD VALIDATE OTP
				request
				.get('/otp/verify/' + validReferralJson.phoneNumber + '/' + result.body.password)
				.end(function(err, result) {
					assert.equal(result.body.check, true);
					// FOURTH DELETE
					request
					.delete('/register/delete/' +  validRegisterJson.businessId + '/' + validRegisterJson.phoneNumber)
					.end(function(err, result) {
						assert.equal(result.body.id, validRegisterJson.phoneNumber);
						done();
					})
				})
			})
		})
	});
});

const invalidRegisterJson = {
	"businessId": "1",
	"phoneNumber": "9988998897"
}

const invalidReferralJson = {
	"businessId": "1",
	"phoneNumber": "9988998897",
	"refPhoneNumber": "9988998897"
}

describe('Otp', function() {
	it('for un-successful operation', function(done) {
		// FIRST INSERT REGISTER
		request
		.post('/register/insert')
		.send(invalidRegisterJson)
		.end(function(err, result) {
			assert.equal(result.body.id, invalidRegisterJson.phoneNumber);
			// SECOND VALIDATE CORE REFERRAL
			request
			.post('/core/validate/referral')
			.send(invalidReferralJson)
			.end(function(err, result) {
				assert.equal(result.body, '');
				// THIRD DELETE
				request
				.delete('/register/delete/' +  invalidRegisterJson.businessId + '/' + invalidRegisterJson.phoneNumber)
				.end(function(err, result) {
					assert.equal(result.body.id, invalidRegisterJson.phoneNumber);
					done();
				})
			})
		})
	});
});
